﻿using UnityEngine;
using System.Collections;
using Nodify.Runtime;

namespace Nodify.Runtime.Nodes
{
    [CreateMenu("Unity/Application/Reload Level", "Application.ReloadLevel")]
    public class UnityApplicationReloadLevel : Node
    {
        public enum LoadLevelType
        {
            LoadLevel,
            LoadLevelAdditive,
            LoadLevelAsync,
            LoadLevelAdditiveAsync
        }

        [Expose]
        public LoadLevelType loadLevelType = LoadLevelType.LoadLevel;

        protected override void OnExecute()
        {
            switch (loadLevelType)
            {
                case LoadLevelType.LoadLevel:
                    Application.LoadLevel(Application.loadedLevel);
                    break;
                case LoadLevelType.LoadLevelAdditive:
                    Application.LoadLevelAdditive(Application.loadedLevel);
                    break;
                case LoadLevelType.LoadLevelAsync:
                    Application.LoadLevelAsync(Application.loadedLevel);
                    break;
                case LoadLevelType.LoadLevelAdditiveAsync:
                    Application.LoadLevelAdditiveAsync(Application.loadedLevel);
                    break;
            }

            base.OnExecute();
        }
    }
}
