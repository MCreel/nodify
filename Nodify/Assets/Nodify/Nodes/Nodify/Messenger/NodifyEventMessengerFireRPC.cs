using UnityEngine;
using System;
using System.Collections;
using Nodify.Runtime;

namespace Nodify.Runtime.Nodes
{
    [CreateMenu("Nodify/Event Messenger/Fire RPC", "EventMessenger.FireRPC", "Icons/event_messenger_fire_rpc_icon")]
    [RequireComponent(typeof(NetworkView))]
	public class NodifyEventMessengerFireRPC : Node 
	{
        [Expose]
        public RPCMode rpcMode;

        [Expose]
        public string eventName;

		protected override void OnExecute()
		{
            GetComponent<NetworkView>().RPC("OnEventRPC", rpcMode, new object[] { eventName });

			base.OnExecute();
		}

        [RPC]
        public void OnEventRPC(string eventName)
        {
            if (this.eventName == eventName)
            {
                EventMessenger.Fire(eventName, null);
            }
        }
	}
}
